﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using EatDer.Data;
using EatDer.Models;



namespace EatDer.Pages
{
    public class IndexModel : PageModel
    {
        private readonly EatDer.Data.EatDerContext _context;
        public IndexModel(EatDer.Data.EatDerContext context){
            _context=context;
        }
        public IList<EatPost> EatDer {get; set;}
        public async Task OnGetAsync(){
                EatDer = await _context.postList
                .Include(p=>p.postUser).ToListAsync();
                


        }

         
    }
}
