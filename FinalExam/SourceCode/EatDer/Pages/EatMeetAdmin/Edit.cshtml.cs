using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using EatDer.Data;
using EatDer.Models;

namespace EatDer.Pages.EatMeetAdmin
{
    public class EditModel : PageModel
    {
        private readonly EatDer.Data.EatDerContext _context;

        public EditModel(EatDer.Data.EatDerContext context)
        {
            _context = context;
        }

        [BindProperty]
        public EatPost EatPost { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            EatPost = await _context.postList
                .Include(e => e.postUser).FirstOrDefaultAsync(m => m.EatPostID == id);

            if (EatPost == null)
            {
                return NotFound();
            }
           ViewData["EatDerUserId"] = new SelectList(_context.Users, "Id", "Id");
            return Page();
        }

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            _context.Attach(EatPost).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!EatPostExists(EatPost.EatPostID))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return RedirectToPage("./Index");
        }

        private bool EatPostExists(int id)
        {
            return _context.postList.Any(e => e.EatPostID == id);
        }
    }
}
