using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using EatDer.Data;
using EatDer.Models;

namespace EatDer.Pages.EatMeetAdmin
{
    public class DetailsModel : PageModel
    {
        private readonly EatDer.Data.EatDerContext _context;

        public DetailsModel(EatDer.Data.EatDerContext context)
        {
            _context = context;
        }

        public EatPost EatPost { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            EatPost = await _context.postList
                .Include(e => e.postUser).FirstOrDefaultAsync(m => m.EatPostID == id);

            if (EatPost == null)
            {
                return NotFound();
            }
            return Page();
        }
    }
}
