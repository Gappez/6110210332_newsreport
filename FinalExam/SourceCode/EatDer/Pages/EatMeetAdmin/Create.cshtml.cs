using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using EatDer.Data;
using EatDer.Models;

namespace EatDer.Pages.EatMeetAdmin
{
    public class CreateModel : PageModel
    {
        private readonly EatDer.Data.EatDerContext _context;

        public CreateModel(EatDer.Data.EatDerContext context)
        {
            _context = context;
        }

        public IActionResult OnGet()
        {
        ViewData["EatDerUserId"] = new SelectList(_context.Users, "Id", "Id");
            return Page();
        }

        [BindProperty]
        public EatPost EatPost { get; set; }

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            _context.postList.Add(EatPost);
            await _context.SaveChangesAsync();

            return RedirectToPage("./Index");
        }
    }
}