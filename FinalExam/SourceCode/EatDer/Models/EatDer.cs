using System;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Identity;
namespace EatDer.Models
{
    public class EatDerUser : IdentityUser{
        public string FirstName {get; set;}
        public string LastName {get; set; }
        public int age {get; set;}
        public string gender {get; set;}

    }

    public class EatPost {
        public int EatPostID {get; set;}
        public string Locations {get; set;}

        public DateTime MeetTime {get; set;}

        public string Post {get; set;}
       
        public int age {get; set;}
        public string spec {get; set;}
        public string gender {get; set;}
         public string EatDerUserId {get; set;}
        public EatDerUser postUser {get; set;}
    }



}