using EatDer.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
namespace EatDer.Data
{
    public class EatDerContext : IdentityDbContext<EatDerUser>
    {
        public DbSet<EatPost> postList {get; set;}

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlite(@"Data source=EatDer.db");
        }
    }



}